#!/bin/bash

echo "$(grep httpTrafficCompleted logs/*)" | \
	sed 's/logs\///;s/__/ /;s/.txt.*: / /' | \
	sed -r 's#(.* )([0-9]*)( .*)#echo \2  \1 $(date +%m%d%y\\ %T --date @\2) \3#e' | \
        sort | \
	sed -r 's/^[0-9]* //'


# Note: we're using awk's 
#      {command string} | getline {variable}
#    syntax. In this usage, awk executes {command string} in a subshell
#    and getline pipes its output to a variable.
# Note also: for some reason I have not determined, getline will only run
#    once for each unique variation on {command string}.  When more than one
#    log file has the same 'timestamp', the call:
#      fmtdatecmd2 | getline testdate
#    results in testdate == "NULL".  (Testing with other source data did not
#    cause this to recur, so it may be a fluke, or may be related to the fact
#    that the date comes from filenames (the only thing I can think of, crazy
#    as it probably is)).
#    So, we're adding a unique no-op line 
#      ";d=" ctr
#    to each command as a workaround.
### echo "$(grep httpTrafficCompleted logs/*)" | \
###       sed 's/logs\///;s/__/ /;s/.txt.*: / /' | \
###       sed -r 's%([0-9]+%
### 
### 	   BEGIN {
###              # fmtdatecmd="date +%m%d%y\\ %T --date @1394562151"
### 	     ctr = 1
###            }
###            {
###              testdate = "NULL";
### 	     ctr = ctr + 1
### 	     fmtdatecmd2 = "";
### 	     fmtdatecmd2="date +%m%d%y\\ %T --date @"($2)
### 	     fmtdatecmd2=fmtdatecmd2 ";d=" ctr
### 	     fmtdatecmd2 | getline testdate 
### 	     #print "----"
### 
### 	     print strftime("%T",$2), $1, $2, testdate, $3
###            }'

