Phantomas Runner
=========


Two VERY simple scripts for 

* running phantomas tests in a continuous cycle
* extracting series page-load times -- quickly see how things are working

This is a demonstration/exploration project, built to handle a specific situation of a 
site manager who was suddenly seeing pronounced, but intermittent, performance problems.  
Although it works, and does offer a means of 
having a very simple, yet (thanks to the awesome phantomas), very powerful, monitoring 
solution in place with a minimum of fuss, this is not a production-ready tool, and there 
are almost certainly more robust and full-featured solutions available.  See [grunt-phantomas](http://4waisenkinder.de/blog/2013/12/22/how-to-measure-frontend-performance-with-phantomas-and-grunt/)
for one intriguing option.



## Requirements

* [NodeJS](http://nodejs.org)
* [PhantomJS 1.9](http://phantomjs.org/)
* [phantomas](https://raw.github.com/macbre/phantomas)

## Installation

```
git clone git@bitbucket.org:jaskho/phantomas-runner.git
```

## Features

* Continuously run phantomas against a collection of urls
* Phantomas metrics logged to url- and timestamp-encoded log files
* View "total load time" series

## Configuration

Edit the testtimer.sh file to supply configuration information:

```
# + + + + +    CONFIGURATION SETTINGS   + + + + + +

# URLS: bash array of urls to test. Provide full uris.
URLS=(http://communitywarehouse.org http://communitywarehouse.org/aboutus)

# FREQ: {integer} Frequency for test runs, in seconds.  Script will test $URLS every $FREQ seconds.
FREQ=300

# MAXRUNS: {integer} Max number of cycles to run. Use MAXRUNS=0 to run indefinitely. MAXRUNS is ignored if ENDTIME is specified.
MAXRUNS=0

# STARTTIME: {POSIX timestamp} Time at which to begin running tests.  Use STARTTIME=0 to begin testing immediately.
STARTTIME=0

# ENDTIME: {POSIX timestamp} Time at which to quit running tests.  Use ENDTIME=0 to begin testing immediately.
ENDTIME=0

# + + + + + + + + + + + + + + + + + + + + + + + + +
```

## "Planned" (read "idly fantasized") extensions

* Pass configuration as parameters
* Support all appropriate phantomas options
* Scheduled start/stop times
* Deployment tool integration
* Expanded (though still very basic) reporting tools
* Reporting (StatsD?) integration

